exports.createProduct = (req, res, next) => {
  res.json({
    message: "Create Product Success!!",
    data: {
      id: 1,
      nama: "Yogi",
      price: 9000,
    },
  });
  next();
};

exports.readProduct = (req, res, next) => {
  res.json({
    message: "Get All Product Success",
    data: {
      id: 1,
      nama: "Yoga",
      price: 100000,
    },
  });
  next();
};
